// Author: Umesh Mohan (moh@nume.sh)
#include "GS3camera.h"

void handle_error(FlyCapture2::Error error) {
    if (error != FlyCapture2::PGRERROR_OK) {
        error.PrintErrorTrace();
        std::terminate();
    }
}

GS3camera::GS3camera (unsigned int serial_number) {
    handle_error(bus_manager.GetCameraFromSerialNumber(serial_number, &guid));
    camera.Connect(&guid);
}

bool GS3camera::is_connected () {
    return camera.IsConnected();
}

float GS3camera::get_property(FlyCapture2::PropertyType property_type) {
    FlyCapture2::Property property;
    property.type = property_type;
    handle_error(camera.GetProperty(&property));
    return property.absValue;
}

float GS3camera::set_property(FlyCapture2::PropertyType property_type, float value) {
    FlyCapture2::PropertyInfo property_info;
    property_info.type = property_type;
    handle_error(camera.GetPropertyInfo(&property_info));
    assert(property_info.absMin <= value <= property_info.absMax);
    FlyCapture2::Property property;
    property.absControl = true;
    property.absValue = value;
    property.type = property_type;
    property.onOff = true;
    camera.SetProperty(&property);
    handle_error(camera.GetProperty(&property));
    return get_property(property_type);
}

void GS3camera::set_strobe_out(int pin_number) {
    FlyCapture2::StrobeControl strobe_control;
    strobe_control.delay = 0;
    strobe_control.duration = 0;
    strobe_control.onOff = false;
    strobe_control.polarity = 0;
    for (int i=1; i<=3; i++) {
        strobe_control.source = i;
        camera.SetStrobe(&strobe_control);
    }
    strobe_control.onOff = true;
    if (pin_number == 2 ||pin_number == 3) {
        strobe_control.source = pin_number;
        camera.SetStrobe(&strobe_control);
    }
}

void GS3camera::set_strobe_in(int pin_number) {
    FlyCapture2::TriggerMode trigger_mode;
    trigger_mode.parameter = 0;
    trigger_mode.polarity = 0;
    if (pin_number == 2 || pin_number == 3) {
        trigger_mode.onOff = true; //External trigger
        trigger_mode.mode = 14;
        trigger_mode.source = pin_number;
    } else {
        trigger_mode.onOff = false; //Internal trigger
        trigger_mode.mode = 0;
        trigger_mode.source = 4; //None
    }
    camera.SetTriggerMode(&trigger_mode);
}

void GS3camera::reset() {
    FlyCapture2::FC2Config configuration;
    configuration.grabMode = FlyCapture2::GrabMode::BUFFER_FRAMES;
    configuration.grabTimeout = 1000;
    configuration.highPerformanceRetrieveBuffer = true;
    configuration.numBuffers = 20;
    camera.SetConfiguration(&configuration);
    FlyCapture2::Format7Info format7_info;
    format7_info.mode = FlyCapture2::Mode::MODE_0;
    bool mode0_is_supported;
    camera.GetFormat7Info(&format7_info, &mode0_is_supported);
    FlyCapture2::Format7ImageSettings format7_settings;
    format7_settings.pixelFormat = FlyCapture2::PixelFormat::PIXEL_FORMAT_RAW8;
    format7_settings.offsetX = 0;
    format7_settings.offsetY = 0;
    format7_settings.width = format7_info.maxWidth;
    format7_settings.height = format7_info.maxHeight;
    FlyCapture2::Format7PacketInfo format7_packet_info;
    bool format7_setting_is_valid;
    camera.ValidateFormat7Settings(&format7_settings, &format7_setting_is_valid, &format7_packet_info);
    if (format7_setting_is_valid) {
        camera.SetFormat7Configuration(&format7_settings, format7_packet_info.recommendedBytesPerPacket);
    }
    FlyCapture2::EmbeddedImageInfo embedded_image_info;
    embedded_image_info.brightness.onOff = false;
    embedded_image_info.exposure.onOff = false;
    embedded_image_info.frameCounter.onOff = true;
    embedded_image_info.gain.onOff = false;
    embedded_image_info.GPIOPinState.onOff = false;
    embedded_image_info.ROIPosition.onOff = false;
    embedded_image_info.shutter.onOff = false;
    embedded_image_info.timestamp.onOff = true;
    embedded_image_info.whiteBalance.onOff = false;
    camera.SetEmbeddedImageInfo(&embedded_image_info);
    set_brightness(0);
    set_exposure(0);
    set_frame_rate(75);
    set_gain(1);
    set_gamma(1);
    set_shutter(10);
    set_strobe_in(-1);
    set_strobe_out(-1);
}

std::vector <unsigned int> get_available_cameras() {
    FlyCapture2::Error error;
    FlyCapture2::BusManager bus_manager;
    unsigned int n_cameras;
    handle_error(bus_manager.GetNumOfCameras(&n_cameras));
    std::vector <unsigned int> serial_numbers = {};
    for (int i=0; i<n_cameras; i++) {
        unsigned int serial_number;
        handle_error(bus_manager.GetCameraSerialNumberFromIndex(i, &serial_number));
        serial_numbers.push_back(serial_number);
    }
    return serial_numbers;
}