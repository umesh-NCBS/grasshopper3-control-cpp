// Author: Umesh Mohan (moh@nume.sh)
#include "GS3camera.h"
#include "iostream"


int main (int argc, char **argv) {
    GS3camera camera1(18275022);
    FlyCapture2::Image image1;
    FlyCapture2::ImageMetadata image1_metadata;
    FlyCapture2::TimeStamp timestamp1;
    camera1.reset();
    camera1.set_as_slave();
    camera1.start_capture();
    camera1.get_image(&image1);
    camera1.stop_capture();
    image1_metadata = image1.GetMetadata();
    timestamp1 = image1.GetTimeStamp();
    std::cout << timestamp1.seconds + (timestamp1.microSeconds/1000000) << ' ' << image1_metadata.embeddedFrameCounter << std::endl;
    camera1.disconnect();

    GS3camera camera2(18275005);
    FlyCapture2::Image image2;
    FlyCapture2::ImageMetadata image2_metadata;
    FlyCapture2::TimeStamp timestamp2;
    camera2.reset();
    camera2.set_as_slave();
    camera2.start_capture();
    camera2.get_image(&image2);
    camera2.stop_capture();
    image2_metadata = image2.GetMetadata();
    timestamp2 = image2.GetTimeStamp();
    std::cout << timestamp2.seconds + (timestamp2.microSeconds/1000000) << ' ' << image2_metadata.embeddedFrameCounter << std::endl;
    camera2.disconnect();

    //camera1.stop_capture();
    //camera1.disconnect();
    return 0;
}