// Author: Umesh Mohan (moh@nume.sh)
#ifndef GS3CAMERA_H_
#define GS3CAMERA_H_

#include "flycapture/FlyCapture2.h"
#include "vector"
#include "assert.h"
#include "iostream"

void handle_error(FlyCapture2::Error error);
std::vector <unsigned int> get_available_cameras();

class GS3camera {
private:
    FlyCapture2::Camera camera;
    FlyCapture2::BusManager bus_manager;
    FlyCapture2::PGRGuid guid;
    FlyCapture2::Error error;
    float get_property(FlyCapture2::PropertyType property_name);
    float set_property(FlyCapture2::PropertyType property_type, float value);
    void set_strobe_out(int pin_number);
    void set_strobe_in(int pin_number);
public:
    GS3camera (unsigned int serial_number);
    bool is_connected();
    float set_gain(float value) {return set_property(FlyCapture2::PropertyType::GAIN, value);};
    float get_gain() {return get_property(FlyCapture2::PropertyType::GAIN);};
    float set_shutter(float value) {return set_property(FlyCapture2::PropertyType::SHUTTER, value);};
    float get_shutter() {return get_property(FlyCapture2::PropertyType::SHUTTER);};
    float set_brightness(float value) {return set_property(FlyCapture2::PropertyType::BRIGHTNESS, value);};
    float get_brightness() {return get_property(FlyCapture2::PropertyType::BRIGHTNESS);};
    float set_gamma(float value) {return set_property(FlyCapture2::PropertyType::GAMMA, value);};
    float get_gamma() {return get_property(FlyCapture2::PropertyType::GAMMA);};
    float set_exposure(float value) {return set_property(FlyCapture2::PropertyType::AUTO_EXPOSURE, value);};
    float get_exposure() {return get_property(FlyCapture2::PropertyType::AUTO_EXPOSURE);};
    float set_frame_rate(float value) {return set_property(FlyCapture2::PropertyType::FRAME_RATE, value);};
    float get_frame_rate() {return get_property(FlyCapture2::PropertyType::FRAME_RATE);};
    void reset();
    void set_as_master() {set_strobe_in(-1); set_strobe_out(2);};
    void set_as_slave() {set_strobe_out(-1); set_strobe_in(2);};
    void disconnect() {camera.Disconnect();};
    void start_capture() {camera.StartCapture();};
    void stop_capture() {camera.StopCapture();};
    void get_image(FlyCapture2::Image* image) {camera.RetrieveBuffer(image);}
};

#endif